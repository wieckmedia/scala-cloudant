name := "scala-cloudant"

organization := "com.wieck"

version := "1.5.1"

scalaVersion := "2.12.6"

crossScalaVersions := Seq("2.11.12", "2.12.6")

scalacOptions in ThisBuild ++= Seq(
  "-language:_",
  "-feature",
  "-unchecked",
  "-deprecation")

parallelExecution in Test := false

resolvers += "Wieck Nexus" at "https://nexus.wieck.com/content/groups/public"

publishTo := {
  val nexus = "https://nexus.wieck.com/content/repositories/"
  if (isSnapshot.value)
    Some("Wieck Snapshots" at nexus + "snapshots")
  else
    Some("Wieck Releases"  at nexus + "releases")
}

val testDependencies = Seq(
  "org.scalatest"               %% "scalatest"           % "3.0.1"  % "test",
  "com.typesafe"                 % "config"              % "1.2.1"  % "test")

libraryDependencies ++= testDependencies ++ Seq(
  "org.dispatchhttp"            %% "dispatch-core"       % "1.0.3",
  "io.spray"                    %% "spray-json"          % "1.3.4",
  "com.typesafe.scala-logging"  %% "scala-logging"       % "3.9.0")
