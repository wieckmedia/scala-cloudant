# Overview

scala-cloudant is a Scala library to help you interact with Cloudant's (a CouchDB variant) SAAS database API.

You can use it to create databases, keep design-documents in-sync, get, update, delete and query against Map/Reduce
Views and Lucene Search Indexes.

# Getting Started

## Creating a Database

...

## Basic Operations

### Create

...

### Get

...

### Update

...

### Delete

...

## Creating Design Documents

## Keeping Design Documents Updated

### Views

...

### Indexes

...

## Querying

### Querying Views

    import com.wieck.cloudant._

    object AnimalsView extends ViewDefinition {
      val name = "animals"
      val mapReduce = MapReduce(
        """
        |function(doc) {
        |  emit([doc.diet, doc.name], doc.diet);
        |}
      """.stripmargin
      )
      type Key = (String, String)
      type Value = String
      def query(database: Database, designDocumentId: String) =
        View[Key, Value](database, designDocumentId, name).query
    }

    val designDocument = DesignDocument(views = Seq(AnimalsView))

    for {
      _       <- database.createDesignDocument("design", designDocument)
      results <- AnimalsView.query(database, "design").includeDocs.execute
    } yield results.docsAs[Animal]

### Querying Indexes

    import com.wieck.cloudant._

    object AnimalsIndex extends IndexDefinition {
      val name = "animals"
      val indexFunction = IndexFunction(
        """
          |function(doc) {
          |  index("default", doc.name);
          |  index("default", doc.diet);
          |  index("age", doc.age);
          |  index("diet", doc.diet, { "store": "yes" });
          |}
        """.stripMargin
      )
    }

    val designDocument = DesignDocument(indexes = Seq(AnimalsIndex))

    for {
      _       <- database.createDesignDocument("design", designDocument)
      results <- AnimalsIndex.search(database, "design").query("omnivore").includeDocs.execute[JsObject]
    } yield results.docs[Animal]

## Bulk Operations

### Bulk Create

...

### Bulk Get

...

### Bulk Update

...

### Bulk Delete

...