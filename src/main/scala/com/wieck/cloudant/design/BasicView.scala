package com.wieck.cloudant
package design

case class BasicView(name: String, mapReduce: MapReduce) extends ViewDefinition