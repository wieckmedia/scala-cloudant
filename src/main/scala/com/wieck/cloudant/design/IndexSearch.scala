package com.wieck.cloudant.design

import spray.json.JsonFormat
import com.wieck.cloudant.Database
import com.wieck.cloudant.query.IndexSearchBuilder

// See: https://cloudant.com/for-developers/search/

case class IndexSearch[K: JsonFormat](database: Database, designDocumentName: String,
  indexName: String, builder: IndexSearchBuilder[K]) {

  def execute[V: JsonFormat] = database.searchIndex[K, V](designDocumentName, indexName, builder.options.asQueryParams)

  def includeDocs = copy[K](builder = builder.includeDocs)

  def query(s: String) = copy[K](builder = builder.query(s))

  def query(s: Option[String]) = copy[K](builder = builder.query(s.getOrElse("*:*")))

  def sort(s: String) = copy[K](builder = builder.sort(s))

  def sort(l: List[String]) = copy[K](builder = builder.sort(l))

  def sort(o: Option[String]) = copy[K](builder = builder.sort(o))

  def limit(i: Int) = copy[K](builder = builder.limit(i))

  def bookmark(s: Option[String]) = copy[K](builder = builder.bookmark(s))

  def bookmark(s: String) = copy[K](builder = builder.bookmark(s))
}

object IndexSearch {

  sealed trait SortParameter

  case object NoSortParameter extends SortParameter

  case class StringSortParameter(value: String) extends SortParameter

  case class ListSortParameter(values: List[String]) extends SortParameter

}
