package com.wieck.cloudant.design

case class MapReduce(map: String, reduce: Option[String] = None)
