package com.wieck.cloudant.design

import com.wieck.cloudant.design.indexes.{analyzers, Analyzer}
import com.wieck.cloudant.{CloudantProtocol, Database}
import com.wieck.cloudant.query.IndexSearchBuilder

case class IndexFunction(index: String, analyzer: Analyzer = analyzers.Standard)

class Index(database: Database, designDocumentId: String, name: String) {

  import CloudantProtocol._

  def search = IndexSearch[String](database, designDocumentId, name, IndexSearchBuilder[String])

}

object Index {

  def apply(database: Database, designDocumentId: String, name: String) =
    new Index(database, designDocumentId, name)

}
