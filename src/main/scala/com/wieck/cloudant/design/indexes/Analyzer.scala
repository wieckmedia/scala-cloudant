package com.wieck.cloudant.design.indexes

trait Analyzer {

  def name: String

}


object Analyzer {

  import analyzers._

  def unapply(name: String): Option[Analyzer] = name match {
    case "standard" => Some(Standard)
    case "email" => Some(Email)
    case "keyword" => Some(Keyword)
    case "simple" => Some(Simple)
    case "whitespace" => Some(Whitespace)
    case "classic" => Some(Classic)
    case "arabic" => Some(languageSpecific.Arabic)
    case "armenian" => Some(languageSpecific.Armenian)
    case "basque" => Some(languageSpecific.Basque)
    case "bulgarian" => Some(languageSpecific.Bulgarian)
    case "brazilian" => Some(languageSpecific.Brazilian)
    case "catalan" => Some(languageSpecific.Catalan)
    case "cjk" => Some(languageSpecific.CJK)
    case "chinese" => Some(languageSpecific.Chinese)
    case "czech" => Some(languageSpecific.Czech)
    case "danish" => Some(languageSpecific.Danish)
    case "dutch" => Some(languageSpecific.Dutch)
    case "english" => Some(languageSpecific.English)
    case "finnish" => Some(languageSpecific.Finnish)
    case "french" => Some(languageSpecific.French)
    case "german" => Some(languageSpecific.German)
    case "greek" => Some(languageSpecific.Greek)
    case "galician" => Some(languageSpecific.Galician)
    case "hindi" => Some(languageSpecific.Hindi)
    case "hungarian" => Some(languageSpecific.Hungarian)
    case "indonesian" => Some(languageSpecific.Indonesian)
    case "irish" => Some(languageSpecific.Irish)
    case "italian" => Some(languageSpecific.Italian)
    case "japanese" => Some(languageSpecific.Japanese)
    case "latvian" => Some(languageSpecific.Latvian)
    case "norwegian" => Some(languageSpecific.Norwegian)
    case "persian" => Some(languageSpecific.Persian)
    case "polish" => Some(languageSpecific.Polish)
    case "portuguese" => Some(languageSpecific.Portuguese)
    case "romanian" => Some(languageSpecific.Romanian)
    case "russian" => Some(languageSpecific.Russian)
    case "spanish" => Some(languageSpecific.Spanish)
    case "swedish" => Some(languageSpecific.Swedish)
    case "thai" => Some(languageSpecific.Thai)
    case "turkish" => Some(languageSpecific.Turkish)
    case _ => None
  }
}