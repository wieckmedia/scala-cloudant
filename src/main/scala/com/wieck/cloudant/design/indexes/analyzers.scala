package com.wieck.cloudant.design.indexes

object analyzers {

  object Standard extends Analyzer { val name = "standard" }

  object Email extends Analyzer { val name = "email" }

  object Keyword extends Analyzer { val name = "keyword" }

  object Simple extends Analyzer { val name = "simple" }

  object Whitespace extends Analyzer { val name = "whitespace" }

  object Classic extends Analyzer { val name = "classic" }

  object languageSpecific {

    object Arabic extends Analyzer { val name = "arabic" }

    object Armenian extends Analyzer { val name = "armenian" }

    object Basque extends Analyzer { val name = "basque" }

    object Bulgarian extends Analyzer { val name = "bulgarian" }

    object Brazilian extends Analyzer { val name = "brazilian" }

    object Catalan extends Analyzer { val name = "catalan" }

    object CJK extends Analyzer { val name = "cjk" }

    object Chinese extends Analyzer { val name = "chinese" }

    object Czech extends Analyzer { val name = "czech" }

    object Danish extends Analyzer { val name = "danish" }

    object Dutch extends Analyzer { val name = "dutch" }

    object English extends Analyzer { val name = "english" }

    object Finnish extends Analyzer { val name = "finnish" }

    object French extends Analyzer { val name = "french" }

    object German extends Analyzer { val name = "german" }

    object Greek extends Analyzer { val name = "greek" }

    object Galician extends Analyzer { val name = "galician" }

    object Hindi extends Analyzer { val name = "hindi" }

    object Hungarian extends Analyzer { val name = "hungarian" }

    object Indonesian extends Analyzer { val name = "indonesian" }

    object Irish extends Analyzer { val name = "irish" }

    object Italian extends Analyzer { val name = "italian" }

    object Japanese extends Analyzer { val name = "japanese" }

    object Latvian extends Analyzer { val name = "latvian" }

    object Norwegian extends Analyzer { val name = "norwegian" }

    object Persian extends Analyzer { val name = "persian" }

    object Polish extends Analyzer { val name = "polish" }

    object Portuguese extends Analyzer { val name = "portuguese" }

    object Romanian extends Analyzer { val name = "romanian" }

    object Russian extends Analyzer { val name = "russian" }

    object Spanish extends Analyzer { val name = "spanish" }

    object Swedish extends Analyzer { val name = "swedish" }

    object Thai extends Analyzer { val name = "thai" }

    object Turkish extends Analyzer { val name = "turkish" }

  }

}
