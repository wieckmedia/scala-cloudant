package com.wieck.cloudant
package design

case class DesignDocument(
    views: Seq[ViewDefinition] = Nil,
    indexes: Seq[IndexDefinition] = Nil,
    updateFunctions: Seq[UpdateDefinition] = Nil,
    language: String = "javascript",
    timestamp: Long = System.currentTimeMillis) {

  def contentEquals(other: DesignDocument): Boolean = {
    basic(true) == other.basic(true)
  }

  private def basic(withTimestamp: Boolean): DesignDocument = {
    if (withTimestamp) 
      copy(
        views = views.map { v => BasicView(v.name, v.mapReduce) }.toVector,
        indexes = indexes.map { i => BasicIndex(i.name, i.indexFunction) }.toVector,
        updateFunctions = updateFunctions.map { u => UpdateFunction(u.name, u.updateFunction) }.toVector,
        timestamp = this.timestamp
      )
    else
      copy(
        views = views.map { v => BasicView(v.name, v.mapReduce) },
        indexes = indexes.map { i => BasicIndex(i.name, i.indexFunction) },
        updateFunctions = updateFunctions.map { u => UpdateFunction(u.name, u.updateFunction) }
      )
  }
}
