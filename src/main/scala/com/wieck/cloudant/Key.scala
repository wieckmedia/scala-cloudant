package com.wieck.cloudant

import scala.language.implicitConversions
import spray.json._

sealed trait Key

sealed trait KeyBound

object KeyBounds {

  case object Min extends KeyBound

  case object Max extends KeyBound

}

case class Key1[A: JsonFormat](_1: A) extends Product1[A] with Key

object Key1 {

  implicit def tuple1ToKey1[A: JsonFormat](t: Tuple1[A]): Key1[A] = Key1(t._1)

}

case class Key2[A: JsonFormat, B: JsonFormat](_1: A, _2: B) extends Product2[A, B] with Key

object Key2 {

  implicit def tuple2ToKey2[A: JsonFormat, B: JsonFormat](t: Tuple2[A, B]): Key2[A, B] = Key2(t._1, t._2)

}

case class Key3[A: JsonFormat, B: JsonFormat, C: JsonFormat](_1: A, _2: B, _3: C) extends Product3[A, B, C] with Key

object Key3 {

  implicit def tuple3ToKey3[A: JsonFormat, B: JsonFormat, C: JsonFormat](t: Tuple3[A, B, C]): Key3[A, B, C] =
    Key3(t._1, t._2, t._3)

}
