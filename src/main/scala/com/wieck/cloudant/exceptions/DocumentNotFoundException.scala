package com.wieck.cloudant.exceptions

case class DocumentNotFoundException(dbName: String, id: String)
  extends Exception(s"Document not found ($id) in database ($dbName")
