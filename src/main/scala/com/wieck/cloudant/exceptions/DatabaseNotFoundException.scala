package com.wieck.cloudant.exceptions

case class DatabaseNotFoundException(name: String) extends Exception(s"Database not found: $name")
