package com.wieck.cloudant.responses

case class ReduceResponse[K, V](rows: List[ViewRow[K, V]])