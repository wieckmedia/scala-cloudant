package com.wieck.cloudant

/*----------------------------------------------------------------
 * NOTE: The field names here match the JSON so we don't have to
 * write custom formats for them, since we don't really use them.
 *
 * Please leave the names as-is with underscores unless you want
 * to write the Spray JSON formats for them!
 *----------------------------------------------------------------*/

case class DatabaseInfoOther(data_size: Long)

case class DatabaseInfo(update_seq: String, db_name: String, purge_seq: Long, other: DatabaseInfoOther,
  doc_del_count: Long, doc_count: Long, disk_size: Long, disk_format_version: Long, compact_running: Boolean,
  instance_start_time: String)

