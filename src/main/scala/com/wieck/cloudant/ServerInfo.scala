package com.wieck.cloudant

case class ServerInfo(val couchdb: String, val version: String)
