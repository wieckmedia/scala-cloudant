package com.wieck.cloudant.query

object PaginationDirection extends Enumeration {

  type PaginationDirection = Value

  val Forward = Value("forward")
  val Reverse = Value("reverse")

  def fold(caseInsensitiveName: Option[String],
           default: PaginationDirection.PaginationDirection = Forward): PaginationDirection.PaginationDirection = {
    caseInsensitiveName map(_.toLowerCase()) match {
      case Some("forward") => Forward
      case Some("reverse") => Reverse
      case _ => default
    }
  }

}