package com.wieck.cloudant.query

import spray.json.JsonFormat

case class ViewQueryBuilder[K1: JsonFormat, K2: JsonFormat, K3: JsonFormat](_key: Option[K1],
  _startKey: Option[K2], _endKey: Option[K3], _inclusiveEnd: Option[Boolean], _includeDocs: Option[Boolean],
  _includeDesign: Option[Boolean], _limit: Option[Int], _group: Option[Boolean], _descending: Option[Boolean]) {

  def options: ViewQueryOptions[K1, K2, K3] = ViewQueryOptions(_key, _startKey, _endKey, _inclusiveEnd, _includeDocs,
    _includeDesign, _limit, _group, _descending)

  def key(k: K1) = copy[K1, K2, K3](_key = Some(k))

  def key(k: Some[K1]) = copy[K1, K2, K3](_key = k)

  def startKey[K: JsonFormat](k: K) = copy[K1, K, K3](_startKey = Some(k))

  def endKey[K: JsonFormat](k: K) = copy[K1, K2, K](_endKey = Some(k))

  def includeDesign = copy[K1, K2, K3](_includeDesign = Some(true))

  def descending = copy[K1, K2, K3](_descending = Some(true))

  def includeDocs = copy[K1, K2, K3](_includeDocs = Some(true))

  def inclusiveEnd = copy[K1, K2, K3](_inclusiveEnd = Some(true))

  def limit(n: Int) = copy[K1, K2, K3](_limit = Some(n))

  def group = copy[K1, K2, K3](_group = Some(true))
}

object ViewQueryBuilder {

  def apply[K1: JsonFormat, K2: JsonFormat, K3: JsonFormat] =
    new ViewQueryBuilder[K1, K2, K3](_key = None, _startKey = None, _endKey = None, _inclusiveEnd = None,
      _includeDocs = None, _includeDesign = None, _limit = None, _group = None, _descending = None)

}
