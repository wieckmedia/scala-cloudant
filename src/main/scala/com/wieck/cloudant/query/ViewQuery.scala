package com.wieck.cloudant.query

import spray.json.JsonFormat
import com.wieck.cloudant.Database

case class ViewQuery[K1: JsonFormat, V: JsonFormat, K2: JsonFormat, K3: JsonFormat](database: Database,
  designDocumentId: String, viewName: String, builder: ViewQueryBuilder[K1, K2, K3]) {

  def execute = database.queryView[K1, V](designDocumentId, viewName, builder.options.asQueryParams)

  def reduce = database.reduceView[K1, V](designDocumentId, viewName, builder.options.asQueryParams)

  def page: PaginatedViewQuery[K1, V, K2, K3] = PaginatedViewQuery[K1, V, K2, K3](database, designDocumentId, viewName, builder)

  def includeDocs = copy[K1, V, K2, K3](builder = builder.includeDocs)

  def descending = copy[K1, V, K2, K3](builder = builder.descending)

  def inclusiveEnd = copy[K1, V, K2, K3](builder = builder.inclusiveEnd)

  def key(k: K1) = copy[K1, V, K2, K3](builder = builder.key(k))

  def key(k: Some[K1]) = copy[K1, V, K2, K3](builder = builder.key(k))

  def startKey[K: JsonFormat](k: K) = copy[K1, V, K, K3](builder = builder.startKey(k))

  def endKey[K: JsonFormat](k: K) = copy[K1, V, K2, K](builder = builder.endKey(k))

  def limit(n: Int) = copy[K1, V, K2, K3](builder = builder.limit(n))

  def group = copy[K1, V, K2, K3](builder = builder.group)

}
