package com.wieck.cloudant.test.integration

import com.wieck.cloudant._
import com.wieck.cloudant.exceptions._
import com.wieck.cloudant.test.support._

class DeleteDocumentSpec extends IntegrationSpec {

  describe("Deleting a document") {

    describe("when the document already exists") {

      it("succeeds") {
        withDatabase { database =>
          val document = await(database.putDocument(NewDocument("aardvark", Animals.aardvark)))
          await(database.deleteDocument("aardvark", document.rev))
        }
      }

    }

    describe("when the document does not exist") {

      it("raises a DocumentNotFoundException") {
        withDatabase { database =>
          intercept[DocumentNotFoundException] {
            await(database.deleteDocument("foo", "123"))
          }
        }
      }

    }

    describe("when the document exists but the rev is wrong") {

      it("raises a DocumentConflictException") {
        withDatabase { database =>
          await(database.putDocument(NewDocument("aardvark", Animals.aardvark)))
          intercept[DocumentConflictException] {
            await(database.deleteDocument("aardvark", "1-caa9467dbcfc3dc6cacd4aaaa550b038"))
          }
        }
      }

    }

  }

}
