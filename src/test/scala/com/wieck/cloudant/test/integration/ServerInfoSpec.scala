package com.wieck.cloudant.test.integration

import com.wieck.cloudant.test.support.IntegrationSpec

class ServerInfoSpec extends IntegrationSpec {

  describe("Querying server info") {

    val serverInfo = await(client.serverInfo)

    it("contains the welcome message") {
      serverInfo.couchdb should equal("Welcome")
    }

    it("contains the server version") {
      serverInfo.version should fullyMatch regex """\d+\.\d+\.\d+"""
    }

  }

}
