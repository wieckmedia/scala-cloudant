package com.wieck.cloudant.test.integration

import com.wieck.cloudant._
import com.wieck.cloudant.exceptions._
import com.wieck.cloudant.test.support._

class PutDocumentSpec extends IntegrationSpec {

  describe("Creating/updating a document") {

    it("returns a RevedDocument") {
      withDatabase { database =>
        val revedDocument = await(database.putDocument(NewDocument("aardvark", Animals.aardvark)))
        revedDocument.isInstanceOf[Document[Animal]] should equal(true)
      }
    }

    describe("when the document already exists") {

      it("raises an exception") {
        withDatabase { database =>
          await(database.putDocument(NewDocument("aardvark", Animals.aardvark), Some(3)))
          intercept[DocumentConflictException] {
            await(database.putDocument(NewDocument("aardvark", Animals.aardvark), Some(3)))
          }
        }
      }

    }

  }

}
