package com.wieck.cloudant.test.integration

import com.wieck.cloudant._
import com.wieck.cloudant.design._
import com.wieck.cloudant.exceptions._
import com.wieck.cloudant.test.support._
import com.wieck.cloudant.design.indexes.{analyzers, PerFieldAnalyzer}
import com.wieck.cloudant.design.IndexFunction
import spray.json.DeserializationException

class DesignDocumentSpec extends WordIntegrationSpec {

  "Creating" when {

    "it doesn't already exist" must {
      "succeed" in {
        withDatabase { database =>
          val designDocument = DesignDocument()
          await(database.createDesignDocument("myDesign", designDocument))
        }
      }
    }

    "it already exists" must {
      "raise a DocumentConflictException" in {
        withDatabase { database =>
          val designDocument = DesignDocument()
          await(database.createDesignDocument("myDesign", designDocument))
          intercept[DocumentConflictException] {
            await(database.createDesignDocument("myDesign", designDocument))
          }
        }
      }
    }

    "a conflicting design is invalid" must {
      "raise a DeserializationException during createOrUpdateDesignDocument" in {
        withDatabase { database =>
          val reference = NewDocument("_design/myDesign", Map("views" -> Map.empty[String, String]))
          await(database.putDocument(reference, Some(3)))
          intercept[DeserializationException] {
            val update = DesignDocument()
            await(database.createOrUpdateDesignDocument("myDesign", update))
          }
        }
      }
    }
  }


  "Comparing" when {
    object AnimalsIndex extends IndexDefinition {

      val name = "animals"

      val indexFunction = IndexFunction(
        """
          |function(doc) {
          |  index("default", doc.name);
          |  index("default", doc.diet);
          |  index("age", doc.age);
          |  index("diet", doc.diet, { "store": "yes" });
          |}
        """.stripMargin,
        analyzer = PerFieldAnalyzer(
          default = analyzers.Whitespace,
          fields = Map("name" -> analyzers.Simple, "diet" -> analyzers.Classic))
      )
    }

    object DummyUpdate extends UpdateDefinition {
      val name = "dummy"
      val updateFunction =
        """
          |function(doc, req) {
          |  return [null, 'nothing']
          |}
        """.stripMargin
    }

    val Dummy2Update = UpdateFunction(name = "dummy2", updateFunction = "function(doc, req) { return [null, 'dummy2'] }")

    "contents match" must {
      "compare equally" in {
        withDatabase { database =>
          val name = "myDesign"
          val reference = DesignDocument(indexes = List(AnimalsIndex), updateFunctions = List(DummyUpdate, Dummy2Update))

          val RevisedDocument(id1, design1, rev1) = await(database.createDesignDocument(name, reference))
          val RevisedDocument(id2, design2, rev2) = await(database.createOrUpdateDesignDocument(name, reference))

          reference contentEquals(design1) should equal(true)
          reference contentEquals(design2) should equal(true)

          id1 should equal(id2)
          rev1 should equal(rev2)
        }
      }
    }

    "contents don't match" must {
      "compare differently" in {
        withDatabase { database =>
          val name = "myDesign"
          val reference = DesignDocument(indexes = List(AnimalsIndex), updateFunctions = List(DummyUpdate))

          val RevisedDocument(id1, design1, rev1) = await(database.createDesignDocument(name, reference))
          val RevisedDocument(id2, design2, rev2) = await(database.createOrUpdateDesignDocument(name, reference))

          reference contentEquals(design1) should equal(true)
          reference contentEquals(design2) should equal(true)

          id1 should equal(id2)
          rev1 should equal(rev2)
        }
      }
    }
  }


  "Retrieving" when {

    "it is valid" must {
      "succeed" in {
        withDatabase { database =>
          val RevisedDocument(referenceId, reference, referenceRev) =
            await(database.createDesignDocument("myDesign", DesignDocument()))
          val RevisedDocument(storedId, stored, storedRev) =
            await(database.client.getDesignDocument(database.name, "myDesign"))

          referenceId should equal(storedId)
          referenceRev should equal(storedRev)
          reference contentEquals(stored) should equal(true)
        }
      }
    }

    "it is invalid" must {
      "raise a DeserializationException" in {
        withDatabase { database =>

          val reference = NewDocument("_design/myDesign", Map("views" -> Map.empty[String, String]))

          await(database.putDocument(reference, Some(3)))

          intercept[DeserializationException] {
            await(database.client.getDesignDocument(database.name, "myDesign"))
          }
        }
      }
    }
  }

}
