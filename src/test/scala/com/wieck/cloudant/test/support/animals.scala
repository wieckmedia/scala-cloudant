package com.wieck.cloudant.test.support

import com.wieck.cloudant._

case class Animal(name: String, diet: String, age: Int)

object Animal {

  import CloudantProtocol._

  implicit val animalFormat = jsonFormat3(Animal.apply)

}

object Animals {

  val aardvark = Animal("aardvark", "omnivore", 8)

  val badger = Animal("badger", "omnivore", 12)

  val elephant = Animal("elephant", "herbivore", 14)

  val kookaburra = Animal("kookaburra", "carnivore", 6)

}
