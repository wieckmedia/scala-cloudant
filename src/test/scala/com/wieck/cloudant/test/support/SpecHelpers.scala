package com.wieck.cloudant.test.support

import scala.util.Random
import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration

trait SpecHelpers {

  object Focus extends org.scalatest.Tag("focus")

  def await[T](future: Future[T]) = Await.result(future, Duration.Inf)

  def tempDatabaseName =
    "tempdb_" + Random.alphanumeric.take(8).mkString.toLowerCase

  def tempDocumentId =
    "tmp_" + Random.alphanumeric.take(8).mkString.toLowerCase

}
