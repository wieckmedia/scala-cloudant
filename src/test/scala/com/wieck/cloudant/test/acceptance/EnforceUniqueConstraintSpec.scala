package com.wieck.cloudant.test.acceptance

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import com.wieck.cloudant._
import com.wieck.cloudant.test.support._
import com.wieck.cloudant.exceptions._

/**
 * See: http://guide.couchdb.org/draft/cookbook.html#uniqueness and
 * http://kfalck.net/2009/06/29/enforcing-unique-usernames-on-couchdb
 */

class EnforceUniqueConstraintSpec extends AcceptanceSpec {

  import Animals._

  case class AnimalNameConstraint(name: String, ref: String) {

    def key = AnimalNameConstraint.key(name)

  }

  object AnimalNameConstraint {

    def key(name: String) = s"unique-animal-name-$name"

  }

  implicit val animalNameFormat = jsonFormat2(AnimalNameConstraint.apply)

  case class DuplicateAnimalNameException(name: String) extends Exception(s"Duplicate animal name: $name")

  case class AnimalRepository(database: Database) {

    def getAnimalOption(id: String): Future[Option[RevisedDocument[Animal]]] =
      database.getDocumentOption[Animal](id)

    def createAnimal(animal: Animal): Future[RevisedDocument[Animal]] = {
      val animalId = java.util.UUID.randomUUID().toString
      for {
        _ <- verifyAndReserveAnimalNameConstraint(animal.name, animalId)
        doc <- database.putDocument(NewDocument(animalId, animal)) // what if this fails?
      } yield doc
    }

    def updateAnimal(doc: RevisedDocument[Animal]): Future[RevisedDocument[Animal]] =
      for {
        _ <- verifyAndReserveAnimalNameConstraint(doc.data.name, doc.id)
        doc <- database.putDocument(doc) // what if this fails?
      } yield doc

    def deleteAnimal(revisedDocument: RevisedDocument[Animal]): Future[Unit] =
      lookupAnimalNameConstraint(revisedDocument.data.name) flatMap {
        case Some(constraint) => {
          // Delete the constraint reservation after successfully deleting the doc.
          database.deleteDocument(revisedDocument.id, revisedDocument.rev) map { _ =>
            database.deleteDocument(constraint.id, constraint.rev)
          }
        }
        case None => database.deleteDocument(revisedDocument.id, revisedDocument.rev)
      }

    private def lookupAnimalNameConstraint(name: String): Future[Option[RevisedDocument[AnimalNameConstraint]]] =
      database.getDocumentOption[AnimalNameConstraint](AnimalNameConstraint.key(name))

    private def verifyAndReserveAnimalNameConstraint(name: String, animalId: String): Future[Unit] =
      lookupAnimalNameConstraint(name) flatMap {
        case Some(constraint) => {
          if (constraint.data.ref == animalId) {
            // The existing reservation matches. Nothing to do.
            Future.successful(())
          } else {
            // Make sure the constraint isn't orphaned.
            getAnimalOption(constraint.data.ref) flatMap {
              case Some(animal) => Future.failed(DuplicateAnimalNameException(name))
              case None => {
                // Delete the orphaned constraint and recreate it.
                database.deleteDocument(constraint.id, constraint.rev) flatMap { _ =>
                  val newConstraint = AnimalNameConstraint(name, animalId)
                  database.putDocument(NewDocument(newConstraint.key, newConstraint)) map (_ => ())
                }
              }
            }
          }
        }
        case None => {
          // Create the constraint.
          val constraint = AnimalNameConstraint(name, animalId)
          database.putDocument(NewDocument(constraint.key, constraint)) map (_ => ()) recoverWith {
            case _: DocumentConflictException => Future.failed(DuplicateAnimalNameException(name))
          }
        }
      }

  }

  feature("Enforce unique constraint") {

    scenario("Creating a new document without conflict") {
      val repository = AnimalRepository(database)

      await(repository.createAnimal(aardvark))
      await(repository.createAnimal(badger))
    }

    scenario("Creating a new document with conflict") {
      val repository = AnimalRepository(database)

      await(repository.createAnimal(aardvark))

      intercept[DuplicateAnimalNameException] {
        await(repository.createAnimal(aardvark))
      }
    }

    scenario("Updating a document without conflict") {
      val repository = AnimalRepository(database)

      val document = await(repository.createAnimal(aardvark))
      await(repository.updateAnimal(document.update(animal => animal.copy(diet = "hungry"))))
    }

    scenario("Updating a document with conflict") {
      val repository = AnimalRepository(database)

      await(repository.createAnimal(aardvark))
      val badgerDocument = await(repository.createAnimal(badger))
      intercept[DuplicateAnimalNameException] {
        await(repository.updateAnimal(badgerDocument.update(animal => animal.copy(name = "aardvark"))))
      }
    }

    scenario("Creating a document with the name of a deleted document") {
      val repository = AnimalRepository(database)

      val document = await(repository.createAnimal(aardvark))
      await(repository.deleteAnimal(document))
      await(repository.createAnimal(aardvark))
    }

  }

}
