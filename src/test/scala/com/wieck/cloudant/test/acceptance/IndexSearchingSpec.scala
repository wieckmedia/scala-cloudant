package com.wieck.cloudant.test.acceptance

import com.wieck.cloudant._
import com.wieck.cloudant.design._
import com.wieck.cloudant.responses.ResponseStreamer
import com.wieck.cloudant.test.support._
import scala.concurrent.ExecutionContext.Implicits.global
import spray.json._

class IndexSearchingSpec extends AcceptanceSpec {

  import Implicits._
  import Animals._

  feature("Index searching") {

    object AnimalsIndex extends IndexDefinition {

      val name = "animals"

      val indexFunction = IndexFunction(
        """
          |function(doc) {
          |  var STORED = { "store": true };
          |  index('default', doc.name);
          |  index('default', doc.diet);
          |  index('name', doc.name, STORED);
          |  index('age', doc.age, STORED);
          |  index('diet', doc.diet, STORED);
          |}
        """.stripMargin)

      def search(database: Database, designDocumentId: String) =
        Index(database, designDocumentId, name).search

    }

    val designDocument = DesignDocument(indexes = Seq(AnimalsIndex))

    def bootstrap =
      await(
        database.bulkPut(
          List(aardvark, badger, elephant, kookaburra) map { animal =>
            NewDocument(animal.name, animal)
          },
          quorum = Some(3)
        ) flatMap (_ => database.createDesignDocument("animals", designDocument))
      )

    scenario("Searching the default index") {
      bootstrap

      val results = await(AnimalsIndex.search(database, "animals").includeDocs.query("omnivore").execute[JsObject])

      results.docs[Animal] should equal(List(badger, aardvark))
    }

    scenario("Search with ascending string sort") {
      bootstrap

      val results = await(AnimalsIndex.search(database, "animals").includeDocs.sort("age").query("omnivore")
        .execute[JsObject])

      results.docs[Animal] should equal(List(aardvark, badger))
    }

    scenario("Search with descending string sort") {
      bootstrap

      val results = await(AnimalsIndex.search(database, "animals").includeDocs.sort("-age").query("omnivore")
        .execute[JsObject])

      results.docs[Animal] should equal(List(badger, aardvark))
    }

    scenario("Search with list sort") {
      bootstrap

      val results = await(AnimalsIndex.search(database, "animals").includeDocs.sort(List("age", "diet<string>")).query("omnivore")
        .execute[JsObject])

      results.docs[Animal] should equal(List(aardvark, badger))
    }

    scenario("Process all documents as a stream across fetches") {
      bootstrap

      import org.scalatest.time.{Span, Seconds}

      val query: IndexSearch[String] = AnimalsIndex.search(database, "animals").query("*:*").limit(2)
      val futureStream = new ResponseStreamer[String, Animal](query).stream

      var count = 0
      whenReady(futureStream, timeout(Span(1, Seconds))) { stream =>
        for (row <- stream) { count += 1 }
      }

      count should equal(4)
    }

  }

}
